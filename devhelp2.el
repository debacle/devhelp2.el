;;; devhelp2.el --- devhelp access in Emacs  -*- lexical-binding: t -*-

;; Copyright (C) 2021 by Martin <debacle@debian.org>
;; Author: Martin <debacle@debian.org>
;; URL: http://salsa.debian.org/debacle/devhelp2.el
;; Created: 2021-07-27
;; Version: 0.1
;; Keywords: devhelp

;; This file is not part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This package provides the function `devhelp2` to access devhelp
;; help inside of Emacs - in contrast to spawn the devhelp GUI
;; program. Therefore it works perfectly well on the console.

;;; Usage:

;;; 1. Bind it to a key, e.g.
;;;    (define-key prog-mode-map [f7] 'devhelp2)

;;; 2. In case the rendering looks ugly, try setting
;;;    devhelp2-shr-use-colors to t.

;; Alternatives to devhelp2:

;; 1. https://gitlab.gnome.org/GNOME/devhelp/-/blob/master/plugins/devhelp.el
;;    2001-2017 Richard Hult <richard@imendio.com> and others
;;    invokes the devhelp program from Emacs as external program

;; 2. https://github.com/evmar/devhelp-index
;;    2011-2012 Evan Martin <martine@danga.com>
;;    needs Python 2 helper scripts

;;; Code:

(require 'eww)
(require 'find-lisp)
(require 'thingatpt)
(require 'xml)

(defcustom devhelp2-shr-use-colors nil
  "If non-nil, respect color specifications in the HTML."
  :type 'boolean)

(defvar devhelp2-idx '())
(defvar devhelp2-dirs '("/usr/share/gtk-doc/"
			"/usr/share/devhelp/books/"
			"/usr/share/doc/"))

(defun devhelp2-populate ()
  "Parse all .devhelp2 index files and store mappings between
symbols and help locations."
  (let ((idx-files (mapcan
		    (lambda (dirname)
		      (if (file-directory-p dirname)
			  (find-lisp-find-files dirname ".*\.devhelp2$")))
		    devhelp2-dirs)))
    (dolist (idx-file idx-files)
      (let* ((dirname (file-name-directory idx-file))
	     (parse-tree (xml-parse-file idx-file))
	     (book-node (assq 'book parse-tree))
	     (functions-node (car (xml-get-children book-node 'functions))))
	(dolist (keyword (xml-get-children functions-node 'keyword))
	  (let* ((attrs (xml-node-attributes keyword))
		 (type (cdr (assq 'type attrs)))
		 ; remove 'enum ' or 'struct ' prefix and '()' or ' ()' suffix
		 (name (replace-regexp-in-string "\\(\\(enum\\|struct\\) +\\)*\\([^ ()]*\\).*"
						 "\\3" (cdr (assq 'name attrs))))
		 (link (cdr (assq 'link attrs))))
	    (if (member type '("constant" "enum" "function" "macro" "struct"))
		(push (cons name (concat dirname link)) devhelp2-idx))))))))

(defun devhelp2 ()
  "Open devhelp page for symbol at point."
  (interactive)
  (if (not devhelp2-idx)
      (devhelp2-populate))
  (let* ((symbol (thing-at-point 'symbol t))
	 (buffer-name (concat "devhelp2:" symbol)))
    (if (get-buffer buffer-name)
	(switch-to-buffer buffer-name)
      (let ((path (cdr (assoc symbol devhelp2-idx))))
	(if path
	    (let ((shr-use-colors devhelp2-shr-use-colors))
	      (eww-browse-url (concat "file://" (expand-file-name path)) t)
	      (rename-buffer buffer-name t))
	  (message (format "symbol %s not found" symbol)))))))

(provide 'devhelp2)
